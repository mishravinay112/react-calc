const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    entry: [
        path.resolve(__dirname, 'src', 'index.js')
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [new htmlWebpackPlugin({
        template: './index.html'
    })]
};

module.exports = config;
