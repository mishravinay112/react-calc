# react-calc

```
git clone https://gitlab.com/mishravinay112/react-calc.git
cd react-calc

yarn
```
but still if you don't want to use `yarn`. you can install dependency via `npm`

`npm i`

To build the dev environment with hot reloading of JS and CSS, type:

`npm start`

By default, the site is available at http://localhost:8080.



If you are suggesting a major overhaul of some aspect of this project, please submit an issue with your suggestion on `mishravinay112@gmail.com`.

## Tools Included in this Project

The tools included in this project are:

- React (of course)
- webpack (bundling assets)
- babel (transpiling code)