import React, { Fragment } from 'react';
import Button from './button';
import PropTypes from 'prop-types';

const styles = {
    section: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    }
}

const CalculatorButtonWrapper = ({ handleButtonClick }) => (
    <Fragment>
        <div style={styles.section}>
            <Button label="7" handleClick={handleButtonClick} />
            <Button label="8" handleClick={handleButtonClick} />
            <Button label="9" handleClick={handleButtonClick} />
            <Button label="/" handleClick={handleButtonClick} />
        </div>
        <br />
        <div style={styles.section}>
            <Button label="4" handleClick={handleButtonClick} />
            <Button label="5" handleClick={handleButtonClick} />
            <Button label="6" handleClick={handleButtonClick} />
            <Button label="*" handleClick={handleButtonClick} />
        </div>
        <br />
        <div style={styles.section}>
            <Button label="1" handleClick={handleButtonClick} />
            <Button label="2" handleClick={handleButtonClick} />
            <Button label="3" handleClick={handleButtonClick} />
            <Button label="-" handleClick={handleButtonClick} />
        </div>
        <br />
        <div style={styles.section}>
            <Button label="0" handleClick={handleButtonClick} />
            <Button label="." handleClick={handleButtonClick} />
            <Button label="=" handleClick={handleButtonClick} />
            <Button label="+" handleClick={handleButtonClick} />
        </div>
    </Fragment>
);

CalculatorButtonWrapper.propTypes = {
    handleButtonClick: PropTypes.func.isRequired,
};

export default CalculatorButtonWrapper;
