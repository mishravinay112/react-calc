import React, { Component } from 'react';
import CalcButtonWrapper from './calcButtonWrapper';
import Input from './input';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
            result: ''
        }

        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.calculate = this.calculate.bind(this);
    }

    calculate() {
        const { inputValue } = this.state;
        
        try {
            const result = (eval(inputValue) || "" ) + "";
            this.setState({
                result,
                inputValue: result
            });
        } catch (err) {
            this.setState({
                result: 'invalid'
            })
        }
    }

    handleButtonClick(event) {
        const  { inputValue } = this.state;

        if (event.target.value === '=') {
            this.calculate();
        } else {
            this.setState({
                inputValue: inputValue.concat(event.target.value)
            });
        }
    }

    handleInputChange(event) {
        console.log(event.target.value);
        this.setState({
            inputValue: event.target.value 
        });
    }

    render() {
        const { inputValue, result } = this.state;
        return (
            <div>
                <Input value={inputValue} handleChange={this.handleInputChange} result={result} />
                <br />
                <CalcButtonWrapper handleButtonClick={this.handleButtonClick} />
            </div>
        ); 
    } 
}

export default App;
