import React from 'react';
import PropTypes from 'prop-types';

const styles = {
    wrapper: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    input: {
        width: '500px',
        height: '100px',
        fontSize: '50px',
    },
    error: {
        border: '1px solid red'
    }
};

const Input = ({ handleChange, value = '', result }) => (
    <div style={styles.wrapper}>
        <input 
            value={value}
            onChange={e => handleChange(e)}
            style={
                result === 'invalid' ? 
                { ...styles.input, ...styles.error} :
                styles.input}
            />
    </div>
);

Input.propTypes = {
    value: PropTypes.string.isRequired,
    result: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
};

export default Input;
