import React from 'react';
import PropTypes from 'prop-types';

const styles = {
    button: {
        display: 'block',
        width: '100',
        height: '100',
    },
    base: {
        margin: '10px'
    }
};

const Button = ({  label, handleClick }) => (
    <div style={styles.base}>
        <button style={styles.button} onClick={e => handleClick(e)} value={label}>{label}</button>
    </div>
);

Button.propTypes = {
    label: PropTypes.number.isRequired,
    handleClick: PropTypes.func.isRequired,
};

export default Button;
